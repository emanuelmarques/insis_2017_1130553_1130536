package insis.formtype;

import org.activiti.engine.form.AbstractFormType;

public class ProductFormType extends AbstractFormType {

	public static final String TYPE_NAME = "Product";

	public String getName() {

		return TYPE_NAME;
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		return arg0;
	}

	@Override
	public String convertModelValueToFormValue(Object modelValue) {

		if (modelValue == null) {
			return null;
		}

		return modelValue.toString();
	}

}
