package insis.formtype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.activiti.engine.form.AbstractFormType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SubProductsListFormType extends AbstractFormType {
	public static final String TYPE_NAME = "subproduct";

	public String getName() {

		return TYPE_NAME;
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		return arg0;
	}

	@Override
	public String convertModelValueToFormValue(Object modelValue) {
		if (modelValue == null) {
			return null;
		}

		return modelValue.toString();
	}
}
