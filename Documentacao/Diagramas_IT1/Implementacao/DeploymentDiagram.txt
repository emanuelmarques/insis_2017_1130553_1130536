@startuml
node UserDevice {
    node Browser 
}

node Server2 {
    node BPS{
        component Explorer
        component Carbon
    }
}

node Server3{
    node IIS{
        node OrderService
    }
}

node Server4{
    node Tomcat{
        node ProductService
    }
}

node Server5{
    database OrderDatabase{
        frame Order
    }
}

node Server6{
    database ProductDatabase{
        frame Product
        frame Task
    }
}

Browser -(0- Explorer

Carbon -0)- Explorer

OrderService -0)- Carbon
ProductService -0)- Carbon

OrderDatabase -0)- OrderService
ProductDatabase -0)- ProductService

@enduml