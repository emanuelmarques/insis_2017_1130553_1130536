@startuml

title Domain Model

object Order {

    Quantity
    Status
    Creation Date
}

object OrderStatus{
    Created
    Accepted
    Rejected
}

object Product{
    Name
    Description
}

object Sequence{
}

object Task{
    Name
    Description
    Time Cost
    Production Cost
}

object Employee{
}
object Administrator{
}
object InternalManagementEmployee {
}
object FinancialDepartmentEmployee{
}
object ProductionEmployee{
}
object ComercialEmployee{
}

Order"*" o- "1"OrderStatus
Order"1" o- "1"Product
Product"1" *-- "1"Sequence
Sequence "1" *-- "*" Task

Administrator --> Employee: extends
InternalManagementEmployee --> Employee: extends
FinancialDepartmentEmployee --> Employee: extends
ProductionEmployee -->Employee: extends
ComercialEmployee -->Employee: extends

Administrator --- Order: aproves
InternalManagementEmployee --- Order: creates
ComercialEmployee --- Order: creates
Administrator --- FinancialDepartmentEmployee: asks information
Administrator --- ProductionEmployee: asks information


@enduml