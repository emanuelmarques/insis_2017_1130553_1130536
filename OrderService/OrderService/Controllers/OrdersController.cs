﻿namespace OrderService.Controllers
{
    using DomainLibrary.Enums;
    using DomainLibrary.Model;
    using OrderService.Repository;
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Web.Http;
    using System.Web.Http.Description;

    public class OrdersController : ApiController
    {
        private IOrderRepository orderRepository = new OrderRepository();

        // GET: api/Orders
        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Order> GetOrders()
        {
            return orderRepository.GetOrders();
        }

        // GET: api/Orders/5
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(int id)
        {
            var order = orderRepository.GetOrder(id);

            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        /// <summary>
        /// Puts the order.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        [ResponseType(typeof(Order))]
        public IHttpActionResult PutOrder(
            [FromUri] int id,
            [FromBody] Order sent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Order order = orderRepository.GetOrder(id);
            if (order == null)
            {
                return NotFound();
            }

            order.ProductId = sent.ProductId;
            order.Quantity = sent.Quantity;
            order.Status = sent.Status;
            orderRepository.UpdateOrder(order);

            try
            {
                orderRepository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(sent.OrderId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Content(HttpStatusCode.OK, order);
        }

        // POST: api/Orders
        /// <summary>
        /// Posts the order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        [ResponseType(typeof(Order))]
        public IHttpActionResult PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (order == null)
            {
                return BadRequest();
            }

            if (!order.CreationDate.HasValue)
            {
                order.CreationDate = DateTime.UtcNow;
            }

            orderRepository.AddOrder(order);
            orderRepository.Save();

            return CreatedAtRoute("DefaultApi", new { id = order.OrderId }, order);
        }

        // DELETE: api/Orders/5
        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(int id)
        {
            Order order = orderRepository.GetOrder(id);
            if (order == null)
            {
                return NotFound();
            }

            orderRepository.DeleteOrder(order);
            orderRepository.Save();

            return Ok(order);
        }

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Checks if the order exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private bool OrderExists(int id)
        {
            return orderRepository.OrderExists(id);
        }
    }
}