namespace OrderService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "CreationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "CreationDate", c => c.DateTime(nullable: false));
        }
    }
}
