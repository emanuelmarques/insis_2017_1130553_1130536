namespace OrderService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTaskSequence : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Sequence_SequenceId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Sequence_SequenceId");
        }
    }
}
