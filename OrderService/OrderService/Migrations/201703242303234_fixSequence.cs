namespace OrderService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixSequence : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskSequences",
                c => new
                    {
                        TaskSequenceId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.TaskSequenceId);
            
            AddColumn("dbo.Products", "Sequence_TaskSequenceId", c => c.Int());
            CreateIndex("dbo.Products", "Sequence_TaskSequenceId");
            AddForeignKey("dbo.Products", "Sequence_TaskSequenceId", "dbo.TaskSequences", "TaskSequenceId");
            DropColumn("dbo.Products", "Sequence_SequenceId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Sequence_SequenceId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Products", "Sequence_TaskSequenceId", "dbo.TaskSequences");
            DropIndex("dbo.Products", new[] { "Sequence_TaskSequenceId" });
            DropColumn("dbo.Products", "Sequence_TaskSequenceId");
            DropTable("dbo.TaskSequences");
        }
    }
}
