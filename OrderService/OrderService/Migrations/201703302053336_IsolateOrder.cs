namespace OrderService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsolateOrder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Sequence_TaskSequenceId", "dbo.TaskSequences");
            DropForeignKey("dbo.Orders", "OrderProduct_ProductId", "dbo.Products");
            DropIndex("dbo.Orders", new[] { "OrderProduct_ProductId" });
            DropIndex("dbo.Products", new[] { "Sequence_TaskSequenceId" });
            AddColumn("dbo.Orders", "ProductId", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "OrderProduct_ProductId");
            DropTable("dbo.Products");
            DropTable("dbo.TaskSequences");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TaskSequences",
                c => new
                    {
                        TaskSequenceId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.TaskSequenceId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Sequence_TaskSequenceId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductId);
            
            AddColumn("dbo.Orders", "OrderProduct_ProductId", c => c.Int());
            DropColumn("dbo.Orders", "ProductId");
            CreateIndex("dbo.Products", "Sequence_TaskSequenceId");
            CreateIndex("dbo.Orders", "OrderProduct_ProductId");
            AddForeignKey("dbo.Orders", "OrderProduct_ProductId", "dbo.Products", "ProductId");
            AddForeignKey("dbo.Products", "Sequence_TaskSequenceId", "dbo.TaskSequences", "TaskSequenceId");
        }
    }
}
