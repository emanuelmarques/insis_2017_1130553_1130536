﻿namespace OrderService.Repository
{
    using System.Linq;
    using DomainLibrary.Model;

    public interface IOrderRepository
    {
        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <returns></returns>
        IQueryable<Order> GetOrders();

        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Order GetOrder(int id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Orders the exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool OrderExists(int id);

        /// <summary>
        /// Adds the order.
        /// </summary>
        /// <param name="order">The order.</param>
        void AddOrder(Order order);

        /// <summary>
        /// Updates the order.
        /// </summary>
        /// <param name="order">The order.</param>
        void UpdateOrder(Order order);

        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <param name="order">The order.</param>
        void DeleteOrder(Order order);
    }
}
