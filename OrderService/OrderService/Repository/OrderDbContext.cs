namespace OrderService
{
    using System.Data.Entity;

    public partial class OrderDbContext : DbContext
    {
        public OrderDbContext()
            : base("name=OrderDbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public System.Data.Entity.DbSet<DomainLibrary.Model.Order> Orders { get; set; }
    }
}
