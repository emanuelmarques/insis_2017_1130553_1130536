﻿namespace OrderService.Repository
{
    using DomainLibrary.Model;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class OrderRepository : IOrderRepository
    {
        /// <summary>
        /// The database
        /// </summary>
        private OrderDbContext db;

        public OrderRepository()
        {
            db = new OrderDbContext();
        }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IQueryable<Order> GetOrders()
        {
            return db.Orders;
        }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Order GetOrder(int id)
        {
            return db.Orders.Find(id);
        }

        /// <summary>
        /// Adds the order.
        /// </summary>
        /// <param name="order">The order.</param>
        public void AddOrder(Order order)
        {
            db.Orders.Add(order);
        }

        /// <summary>
        /// Updates the order.
        /// </summary>
        /// <param name="order">The order.</param>
        public void UpdateOrder(Order order)
        {
            db.Entry(order).State = EntityState.Modified;
        }

        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <param name="order">The order.</param>
        public void DeleteOrder(Order order)
        {
            //var orderList = db.Orders;
            //foreach (var orderino in orderList)
            //{
            //    db.Orders.Remove(orderino);
            //}
            db.Orders.Remove(order);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        /// <summary>
        /// Checks if the order exists
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.OrderId == id) > 0;
        }
    }
}