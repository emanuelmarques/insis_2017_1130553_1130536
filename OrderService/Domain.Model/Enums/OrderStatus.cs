﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace OrderService.Enums
{
    public enum OrderStatus
    {
        Created = 0,
        Accepted = 1,
        Rejected = 2
    }
}