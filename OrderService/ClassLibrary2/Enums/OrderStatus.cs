﻿namespace DomainLibrary.Enums
{
    public enum OrderStatus
    {
        Created = 0,
        Accepted = 1,
        Rejected = 2
    }
}