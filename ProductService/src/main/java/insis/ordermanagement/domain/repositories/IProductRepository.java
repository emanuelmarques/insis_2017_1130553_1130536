package insis.ordermanagement.domain.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import insis.ordermanagement.domain.model.Product;

@RepositoryRestResource(exported = false)
public interface IProductRepository extends CrudRepository<Product, Long> {
	 Product findById(long id);
}