package insis.ordermanagement.domain.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import insis.ordermanagement.domain.model.Task;

@RepositoryRestResource(exported = false)
public interface ITaskRepository extends PagingAndSortingRepository<Task, Long> {
	 List<Task> findByProductId(long productId);
}