package insis.ordermanagement.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Tasks", uniqueConstraints = @UniqueConstraint(columnNames = {"ProductId", "Number"}))
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;
	
	@Column(name = "ProductId")
	private long productId;
	
	@Column(name = "Number")
	private int numberInSequence;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name = "Description")
	private String description;
	
	@Column(name = "MoneyCost")
	private Double moneyCost;
	
	@Column(name = "TimeCost")
	private Double timeCost;
	
	public Task(){}
	
	
	
	public Task(long productId, 
				int numberInSequence, 
				String name, 
				String description, 
				Double moneyCost,
				Double timeCost) {
		super();
		this.productId = productId;
		this.numberInSequence = numberInSequence;
		this.name = name;
		this.description = description;
		this.moneyCost = moneyCost;
		this.timeCost = timeCost;
	}



	public long getId() {
		return id;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getNumberInSequence() {
		return numberInSequence;
	}

	public void setNumberInSequence(int numberInSequence) {
		this.numberInSequence = numberInSequence;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getMoneyCost() {
		return moneyCost;
	}

	public void setMoneyCost(Double moneyCost) {
		this.moneyCost = moneyCost;
	}

	public Double getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(Double timeCost) {
		this.timeCost = timeCost;
	}	
}
