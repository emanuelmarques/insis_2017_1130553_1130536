package insis.ordermanagement.presentation.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import insis.ordermanagement.application.dto.ProductCostDTO;
import insis.ordermanagement.application.dto.ProductDTO;
import insis.ordermanagement.application.dto.TaskDTO;
import insis.ordermanagement.application.services.task.ITaskService;

@RestController
public class TasksController {

		@Autowired
		private ITaskService taskService;
	
	    @RequestMapping(value = "/tarefas", 
	    				params = "produto",
	    				method = RequestMethod.GET, 
	    				produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<TaskDTO>> getTasksByProductId(
	    		@RequestParam(value="produto", required=true) long produtoId) 
	    {
	        List<TaskDTO> tasks = this.taskService.getByProductId(produtoId);
	        if(tasks == null){
	        	return new ResponseEntity<List<TaskDTO>>(HttpStatus.NOT_FOUND);
	        }
	        
	        return new ResponseEntity<List<TaskDTO>>(tasks, HttpStatus.OK);
	    }
	    
	    @RequestMapping(value = "/tarefas",
				method = RequestMethod.GET, 
				produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<List<TaskDTO>> getTasks() 
		{
		    List<TaskDTO> tasks = this.taskService.getAllTasks();
		    if(tasks == null){
		    	return new ResponseEntity<List<TaskDTO>>(HttpStatus.NOT_FOUND);
		    }
		    
		    return new ResponseEntity<List<TaskDTO>>(tasks, HttpStatus.OK);
		}

	    
	    @RequestMapping(value = "/tarefas", 
	    				method = RequestMethod.POST, 
	    				produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<List<TaskDTO>> postTasks(@RequestBody(required = true) List<TaskDTO> tasks) {
			if(tasks == null || tasks.isEmpty()){
				return new ResponseEntity<List<TaskDTO>>(HttpStatus.BAD_REQUEST);
			}
			
			List<TaskDTO> tasksInserted = this.taskService.insertTasks(tasks);
			
			if (tasksInserted == null || tasksInserted.isEmpty()) {
				return new ResponseEntity<List<TaskDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			return new ResponseEntity<List<TaskDTO>>(tasksInserted, HttpStatus.OK);
	    }
}