package insis.ordermanagement.presentation.web.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import insis.ordermanagement.application.dto.ProductCostDTO;
import insis.ordermanagement.application.dto.ProductDTO;
import insis.ordermanagement.application.dto.TaskDTO;
import insis.ordermanagement.application.services.product.IProductService;
import insis.ordermanagement.application.services.product.ProductService;
import insis.ordermanagement.application.services.task.ITaskService;

@RestController
public class ProductsController {

	@Autowired
	private IProductService productService;
	
	@Autowired
	private ITaskService taskService;

	@RequestMapping(value = "/produto", 
					params = "id", 
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDTO> getProductById(@RequestParam(value = "id", required = true) long id) {
		ProductDTO product = this.productService.getById(id);
		if (product == null) {
			return new ResponseEntity<ProductDTO>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<ProductDTO>(product, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/produto/custos", 
			params = "id", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductCostDTO> getProductCostById(@RequestParam(value = "id", required = true) long id) {
		ProductCostDTO productCost = this.taskService.getProductCostById(id);
		if (productCost == null) {
			return new ResponseEntity<ProductCostDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ProductCostDTO>(productCost, HttpStatus.OK);
	}

	@RequestMapping(value = "/produto", 
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDTO>> getAll() {
		List<ProductDTO> products = this.productService.getAll();
		if (products == null) {
			return new ResponseEntity<List<ProductDTO>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<ProductDTO>>(products, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/produto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDTO> postProduct(@RequestBody(required = true) ProductDTO product) {
		if(product == null || product.getName() == null || product.getDescription() == null){
			return new ResponseEntity<ProductDTO>(HttpStatus.BAD_REQUEST);
		}
		
		ProductDTO productInserted = this.productService.post(product);
		if (productInserted == null) {
			return new ResponseEntity<ProductDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ProductDTO>(productInserted, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filldata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDTO> fillInData() {
		for(int i=1; i<11; i++){
			this.productService.post(
					new ProductDTO(
							i, 
							"Produto" + String.valueOf(i), 
							"Descricao" + String.valueOf(i)));
			ArrayList<TaskDTO> tasks = new ArrayList<>();
			tasks.add(
					new TaskDTO(
							(long) 1,
							(long) i,
							1,
							"Task" + String.valueOf(1), 
							"Descricao" + String.valueOf(1), 
							10.0, 
							10.0));
			this.taskService.insertTasks(tasks);
		}
		return new ResponseEntity<ProductDTO>(HttpStatus.OK);
	}
}