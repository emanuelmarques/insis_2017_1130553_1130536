package insis.ordermanagement.infrastructure.startup;

import java.util.ArrayList;

import org.h2.server.web.WebServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import insis.ordermanagement.application.dto.ProductDTO;
import insis.ordermanagement.application.dto.TaskDTO;
import insis.ordermanagement.application.services.product.ProductService;
import insis.ordermanagement.presentation.web.controllers.ProductsController;
import insis.ordermanagement.presentation.web.controllers.TasksController;

@SpringBootApplication
@EnableJpaRepositories("insis.ordermanagement.domain.repositories")
@EntityScan("insis.ordermanagement.domain.model")
@ComponentScan({"insis.ordermanagement.presentation.web.controllers", "insis.ordermanagement.application.services"})
public class ProductServiceApplication {

	//database URL on H2 Console: jdbc:h2:mem:testdb
	@Bean
	public ServletRegistrationBean h2servletRegistration() {
	    ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
	    registration.addUrlMappings("/console/*");
	    return registration;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}
}
