package insis.ordermanagement.application.dto;

public class ProductDTO {

	private long productId;
	
	private String name;
	
	private String description;
	
	public ProductDTO() {
		super();
	}

	public ProductDTO(
			long productId, 
			String name, 
			String description) {
		super();
		this.productId = productId;
		this.name = name;
		this.description = description;
	}
	
	public long getProductId() {
		return productId;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
