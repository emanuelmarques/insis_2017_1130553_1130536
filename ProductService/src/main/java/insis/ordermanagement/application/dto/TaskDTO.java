package insis.ordermanagement.application.dto;

public class TaskDTO {

	private long taskId;
	
	private long productId;
	
	private int numberInSequence;
	
	private String name;
	
	private String description;
	
	private Double moneyCost;
	
	private Double timeCost;
	
	public TaskDTO() {
		super();
	}

	public TaskDTO(
			long taskId, 
			long productId, 
			int numberInSequence, 
			String name, 
			String description,
			Double moneyCost, 
			Double timeCost) {
		super();
		this.taskId = taskId;
		this.productId = productId;
		this.numberInSequence = numberInSequence;
		this.name = name;
		this.description = description;
		this.moneyCost = moneyCost;
		this.timeCost = timeCost;
	}
	
	public long getTaskId() {
		return taskId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getNumberInSequence() {
		return numberInSequence;
	}

	public void setNumberInSequence(int numberInSequence) {
		this.numberInSequence = numberInSequence;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getMoneyCost() {
		return moneyCost;
	}

	public void setMoneyCost(Double moneyCost) {
		this.moneyCost = moneyCost;
	}

	public Double getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(Double timeCost) {
		this.timeCost = timeCost;
	}
}
