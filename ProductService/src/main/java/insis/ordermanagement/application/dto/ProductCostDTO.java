package insis.ordermanagement.application.dto;

public class ProductCostDTO {

	private long productId;
	private Double monetary; 
	private Double time; 
	
	public ProductCostDTO() {
		super();
	}

	public ProductCostDTO(
			long productId, 
			Double monetary,
			Double time){
		super();
		this.productId = productId;
		this.monetary = monetary;
		this.time = time;
	}
	
	public long getProductId() {
		return productId;
	}

	public Double getMonetary() {
		return monetary;
	}

	public void setMonetary(Double monetary) {
		this.monetary = monetary;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}
}
