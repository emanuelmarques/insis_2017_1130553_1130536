package insis.ordermanagement.application.services.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import insis.ordermanagement.application.dto.ProductCostDTO;
import insis.ordermanagement.application.dto.TaskDTO;
import insis.ordermanagement.domain.model.Task;
import insis.ordermanagement.domain.repositories.ITaskRepository;

@Service
public class TaskService implements ITaskService {

	@Autowired
	private ITaskRepository taskRepository;
	
	public List<TaskDTO> getByProductId(long productId) {
		List<Task> taskModelList = this.taskRepository.findByProductId(productId);
    	if(taskModelList != null && !taskModelList.isEmpty()){
    		return taskModelList.stream()
    				.distinct()
    				.filter(t -> t != null)
    				.map(tm -> new TaskDTO(
        					tm.getId(),
        					tm.getProductId(),
        					tm.getNumberInSequence(),
        					tm.getName(),
        					tm.getDescription(),
        					tm.getMoneyCost(),
        					tm.getTimeCost()))
    				.collect(Collectors.toList());
    	}
    	return null;
	}
	
	public ProductCostDTO getProductCostById(long productId){
		List<Task> taskModelList = this.taskRepository.findByProductId(productId);
		Double monetary = 0.0;
		Double time = 0.0;
		if(taskModelList != null && !taskModelList.isEmpty()){
			for(Task task : taskModelList){
				monetary += task.getMoneyCost();
				time += task.getTimeCost();
			}
					
			return new ProductCostDTO(
					productId,
					monetary,
					time);
		}
		return null;
	}
	
	public List<TaskDTO> insertTasks(List<TaskDTO> tasks) {
		List<Task> taskModelList = tasks.stream()
									.filter(t -> t!= null)
									.map(tm -> new Task(
				        					tm.getProductId(),
				        					tm.getNumberInSequence(),
				        					tm.getName(),
				        					tm.getDescription(),
				        					tm.getMoneyCost(),
				        					tm.getTimeCost()))
				    				.collect(Collectors.toList());
		
		Iterable<Task> insertedTasks = this.taskRepository.save(taskModelList);
		List<TaskDTO> result = new ArrayList<TaskDTO>();
		
		insertedTasks.forEach(t -> 
						result.add(new TaskDTO(
							t.getId(),
							t.getProductId(),
							t.getNumberInSequence(),
							t.getName(),
							t.getDescription(),
							t.getMoneyCost(),
							t.getTimeCost())));
		
		return result;
	}

	@Override
	public List<TaskDTO> getAllTasks() {
		Iterable<Task> tasks = this.taskRepository.findAll();
		List<TaskDTO> taskList = new ArrayList<TaskDTO>();
		for(Task task : tasks){
			taskList.add(
					new TaskDTO(
							task.getId(),
							task.getProductId(),
							task.getNumberInSequence(),
							task.getName(),
							task.getDescription(),
							task.getMoneyCost(),
							task.getTimeCost()));
		}
		return taskList;
	}
}