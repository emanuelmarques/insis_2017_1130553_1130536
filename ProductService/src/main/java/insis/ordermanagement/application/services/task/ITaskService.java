package insis.ordermanagement.application.services.task;

import java.util.List;

import insis.ordermanagement.application.dto.ProductCostDTO;
import insis.ordermanagement.application.dto.TaskDTO;

public interface ITaskService {

	List<TaskDTO> getByProductId(long productId);
	
	List<TaskDTO> getAllTasks();
	
	List<TaskDTO> insertTasks(List<TaskDTO> tasks);
	
	ProductCostDTO getProductCostById(long productId);
}