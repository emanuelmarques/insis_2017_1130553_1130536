package insis.ordermanagement.application.services.product;

import java.util.List;

import insis.ordermanagement.application.dto.ProductDTO;

public interface IProductService {

	ProductDTO getById(long id);
	
	List<ProductDTO> getAll();
	
	ProductDTO post(ProductDTO productDto);
}
