package insis.ordermanagement.application.services.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import insis.ordermanagement.application.dto.ProductDTO;
import insis.ordermanagement.domain.model.Product;
import insis.ordermanagement.domain.repositories.IProductRepository;

@Service
public class ProductService implements IProductService {

	@Autowired
	private IProductRepository productRepository;
	
	public ProductDTO getById(long id) {
    	Product productModel = this.productRepository.findById(id);
    	if(productModel != null){
    		return new ProductDTO(
        			productModel.getId(),
        			productModel.getName(),
        			productModel.getDescription());
    	}
    	return null;
	}
	
	public List<ProductDTO> getAll(){
		List<ProductDTO> result = new ArrayList<ProductDTO>();
		Iterable<Product> products = this.productRepository.findAll();
		products.forEach(p -> 
			result.add(
				new ProductDTO(
					p.getId(),
					p.getName(),
					p.getDescription()
				)
			)
		);
		return result;
	}
	
	public ProductDTO post(ProductDTO productDto){
		Product product = new Product(
				productDto.getName(),
				productDto.getDescription());
		
		Product result = this.productRepository.save(product);
		
		return new ProductDTO(
				result.getId(),
				result.getName(),
				result.getDescription());
	}
}
