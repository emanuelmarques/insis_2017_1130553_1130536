namespace ProcessManager.Models
{
    public class Notification
    {
        public string Type { get; set; }

        public string Format { get; set; }

        public string Proposals_Url { get; set; }

        public string Orders_Url { get; set; }
    }
}