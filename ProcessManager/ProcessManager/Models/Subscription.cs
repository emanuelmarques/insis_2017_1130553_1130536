namespace ProcessManager.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Subscription
    {
        [Key]
        public string Key { get; set; }

        public string Name { get; set; }

        public Notification Notification { get; set; }
    }
}