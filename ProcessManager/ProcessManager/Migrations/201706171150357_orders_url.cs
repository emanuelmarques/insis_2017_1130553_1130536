namespace ProcessManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orders_url : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subscriptions", "Notification_Orders_Url", c => c.String());
            DropColumn("dbo.Subscriptions", "Notification_Order_Url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Subscriptions", "Notification_Order_Url", c => c.String());
            DropColumn("dbo.Subscriptions", "Notification_Orders_Url");
        }
    }
}
